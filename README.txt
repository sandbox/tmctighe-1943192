Mass Taxonomy Merge
===================

Overview
--------
Mass Taxonomy Merge is made to be a tool for doing a one-time mass update of
taxonomy in a system with large amounts of content. Other modules that exist,
while very effective, take considerable amounts of time to run in systems with
large amounts of content. Mass Taxonomy Merge allows the user to upload a
simple CSV file with a list of terms to merge from and merge in to.

Features
--------
Creation of a simple CSV file allows for mass managing of taxonomy terms.
The module can:
 * Create terms
 * Merge existing terms
 * Delete terms

Functionality Not Yet Implemented
---------------------------------
Acts on the database as opposed to loading every entity.
  This means that it could be necessary to hook in to the process to
  ensure that custom taxonomy code does not get out of sync.


Similar projects
----------------
Taxonomy Manager - http://drupal.org/project/taxonomy_manager
Term Merge - http://drupal.org/project/term_merge
