<?php

/**
 * @file
 * File for creating a form to upload the CSV.
 */

/**
 * Implements hook_page().
 */
function mass_taxonomy_merge_page() {
  return drupal_get_form('mass_taxonomy_merge_form');
}

/**
 * Implements hook_form().
 */
function mass_taxonomy_merge_form($form, &$form_state) {
  $form = array();

  $form['options']['alias'] = array(
    '#type' => 'select',
    '#title' => t('URL Aliases'),
    '#description' => t('Should the module effect url aliases of taxonomy pages?'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => 0,
    '#required' => TRUE,
  );

  $file_size = t('Maximum file size: !size MB.', array('!size' => file_upload_max_size() / '1048576'));
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('CSV File'),
    '#size' => 40,
    '#description' => t('Select the CSV file to be processed. ') . $file_size,
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#attributes']['enctype'] = "multipart/form-data";
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function mass_taxonomy_merge_form_validate($form, &$form_state) {
  $validators = array('file_validate_extensions' => array('csv'));
  $dir = 'public://mass_taxonomy_merge';
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  $file = file_save_upload('file', $validators, 'public://mass_taxonomy_merge', FILE_EXISTS_RENAME);

  if (!$file) {
    form_set_error('file', t('A file must be uploaded.'));
  }
  elseif ($file->filemime != 'text/csv') {
    form_set_error('file', t('The file must be of CSV type only.'));
  }
  else {
    $form_state['values']['file'] = $file;
    drupal_set_message(t('File successfully uploaded'), 'status');
  }
}

/**
 * Implements hook_form_submit().
 */
function mass_taxonomy_merge_form_submit($form, &$form_state) {
  $line_max = variable_get('user_import_line_max', 2000);
  $filepath = file_create_url($form_state['values']['file']->uri);
  ini_set('auto_detect_line_endings', TRUE);
  $handle = @fopen($filepath, "r");
  $from = array();
  $to = array();
  $vocab = array();
  $parents = array();
  while ($row = fgetcsv($handle, $line_max, ',')) {
    $from[] = $row[1];
    $to[] = $row[2];
    $vocab[] = $row[3];
    $parents[] = $row[4];
  }

  $use_alias = $form_state['values']['alias'];

  array_shift($from);
  array_shift($to);
  array_shift($vocab);
  array_shift($parents);

  mass_taxonomy_merge_merge($from, $to, $vocab, $parents, $use_alias);
}
